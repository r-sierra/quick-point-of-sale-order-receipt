# -*- coding: utf-8 -*-
{
    'name': "TPV Recibo",
    'summary': "Diseño personalizado del recibo para el Punto de Venta",
    'description': """
Se elimina del recibo:

* Subtotal detallado de impuestos
* Total detallado de impuestos
* Datos de la empresa (CUIT, telefonos, contacto, dirección, usuario)
    """,
    'author': "Roberto Sierra <roberto@ideadigital.com.ar>",
    'website': "https://gitlab.com/r-sierra/quick-point-of-sale-order-receipt",
    'license': 'LGPL-3',
    'category': 'Sales/Point Of Sale',
    'version': '13.0.0.1.0',
    'depends': [
        'point_of_sale',
    ],
    'qweb': [
        'static/src/xml/pos_receipt.xml'
    ],
}
